module Activity exposing
    ( Activity(..)
    , FoodSource(..)
    , PottyType(..)
    , defaultActivity
    , getId
    , idZero
    , isBoth
    , isBottle
    , isFeed
    , isLeft
    , isPee
    , isPoo
    , isPotty
    , isRight
    , timeZero
    , toBottle
    , toFeed
    , toLeft
    , toPee
    , toPotty
    , toRight
    , togglePee
    , togglePoo
    )

import Time exposing (Month, Posix, Weekday, Zone)


timeZero : Posix
timeZero =
    Time.millisToPosix 0


idZero : Int
idZero =
    -1


type FoodSource
    = LeftBreast
    | RightBreast
    | Bottle


type PottyType
    = Pee
    | Poo
    | Both


type Activity
    = Feeding FoodSource Posix Int
    | Potty PottyType Posix Int
    | Unknown


defaultActivity : Activity
defaultActivity =
    Feeding LeftBreast timeZero idZero


false : a -> Bool
false _ =
    False


true : a -> Bool
true _ =
    True


withFeeding : (Activity -> a) -> (FoodSource -> Posix -> Int -> a) -> Activity -> a
withFeeding g f act =
    case act of
        Feeding q t i ->
            f q t i

        _ ->
            g act


mapFeeding : (FoodSource -> Posix -> Int -> Activity) -> Activity -> Activity
mapFeeding =
    withFeeding identity


withPotty : (Activity -> a) -> (PottyType -> Posix -> Int -> a) -> Activity -> a
withPotty g f act =
    case act of
        Potty q t i ->
            f q t i

        _ ->
            g act


mapPotty : (PottyType -> Posix -> Int -> Activity) -> Activity -> Activity
mapPotty =
    withPotty identity


isFeed : Activity -> Bool
isFeed =
    withFeeding false (\_ _ _ -> True)


isPotty : Activity -> Bool
isPotty =
    withPotty false (\_ _ _ -> True)


isLeft : Activity -> Bool
isLeft act =
    case act of
        Feeding LeftBreast _ _ ->
            True

        _ ->
            False


isRight : Activity -> Bool
isRight act =
    case act of
        Feeding RightBreast _ _ ->
            True

        _ ->
            False


isBottle : Activity -> Bool
isBottle act =
    case act of
        Feeding Bottle _ _ ->
            True

        _ ->
            False


isPoo : Activity -> Bool
isPoo act =
    case act of
        Potty Poo _ _ ->
            True

        Potty Both _ _ ->
            True

        _ ->
            False


isPee : Activity -> Bool
isPee act =
    case act of
        Potty Pee _ _ ->
            True

        Potty Both _ _ ->
            True

        _ ->
            False


isBoth : Activity -> Bool
isBoth act =
    case act of
        Potty Both _ _ ->
            True

        _ ->
            False


getId : Activity -> Int
getId act =
    case act of
        Potty _ _ idNum ->
            idNum

        Feeding _ _ idNum ->
            idNum

        Unknown ->
            -1


toFeed : Activity -> Activity
toFeed =
    mapPotty (\_ t i -> Feeding LeftBreast t i)


toLeft : Activity -> Activity
toLeft =
    mapFeeding (\_ t i -> Feeding LeftBreast t i)


toRight : Activity -> Activity
toRight =
    mapFeeding (\_ t i -> Feeding RightBreast t i)


toBottle : Activity -> Activity
toBottle =
    mapFeeding (\_ t i -> Feeding Bottle t i)


toPotty : Activity -> Activity
toPotty =
    mapFeeding (\_ t i -> Potty Pee t i)


toPee : Activity -> Activity
toPee =
    mapFeeding (\_ t i -> Potty Pee t i)


toPoo : Activity -> Activity
toPoo =
    mapFeeding (\_ t i -> Potty Poo t i)


toBoth : Activity -> Activity
toBoth =
    mapFeeding (\_ t i -> Potty Both t i)


togglePee : Activity -> Activity
togglePee =
    mapPotty
        (\pottyType t i ->
            case pottyType of
                Pee ->
                    Potty Pee t i

                Poo ->
                    Potty Both t i

                Both ->
                    Potty Poo t i
        )


togglePoo : Activity -> Activity
togglePoo =
    mapPotty
        (\pottyType t i ->
            case pottyType of
                Pee ->
                    Potty Both t i

                Poo ->
                    Potty Poo t i

                Both ->
                    Potty Pee t i
        )
