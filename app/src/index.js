import './main.css';
import './spinner.css';
import { Elm, Main } from './Main.elm';
import * as serviceWorker from './serviceWorker';

const flagCfg = { api_base_url: process.env.ELM_APP_API_BASE_URL };

Elm.Main.init({
  node: document.getElementById('root'),
  flags: flagCfg
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister(); // TODO switch to register for a real release
