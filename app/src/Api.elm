module Api exposing
    ( delete
    , get
    , patch
    , post
    )

import Http as H


corsReq =
    H.riskyRequest


post : H.Body -> H.Expect msg -> String -> Cmd msg
post =
    mkReq "POST"


get : H.Expect msg -> String -> Cmd msg
get =
    mkReq "GET" H.emptyBody


patch : H.Body -> H.Expect msg -> String -> Cmd msg
patch =
    mkReq "PATCH"


delete : H.Expect msg -> String -> Cmd msg
delete =
    mkReq "DELETE" H.emptyBody


mkReq : String -> H.Body -> H.Expect msg -> String -> Cmd msg
mkReq method bod exp url =
    corsReq
        { method = method
        , headers =
            [ H.header "Access-Control-Allow-Method" method
            ]
        , url = url
        , expect = exp
        , body = bod
        , timeout = Nothing
        , tracker = Nothing
        }
