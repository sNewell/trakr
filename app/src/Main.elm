module Main exposing (main)

import Activity exposing (..)
import Api
import Browser
import Browser.Navigation exposing (load)
import Html exposing (Html, b, button, div, fieldset, form, h1, h2, h3, i, input, label, li, p, span, text, ul)
import Html.Attributes exposing (checked, class, for, id, name, placeholder, src, style, type_)
import Html.Events exposing (onClick, onInput, onSubmit)
import Http exposing (Error)
import Json.Decode as D
import Json.Encode as E
import Task exposing (Task)
import Time exposing (Month, Posix, Weekday, Zone)
import Url
import Url.Builder exposing (absolute)
import Url.Parser exposing ((</>), Parser, fragment, oneOf, parse, top)


type alias StatsDto =
    { average : Int
    , high : Int
    , low : Int
    }


type alias ActivityDto =
    { activity : String
    , qualifier : String
    , time : Int -- Note; a value of -1 defers setting to the server
    , idNum : Int -- Note; a value of -1 defers to the server
    }


type alias AckResponse =
    { message : String
    , success : Bool
    }


type alias AuthResponse =
    { id : Int
    , accountId : Int
    , email : String
    , name : String
    }


type alias ResetPassDto =
    { token : String
    , newPass : String
    , resetCode : String
    }


mkDto : Posix -> Int -> String -> String -> ActivityDto
mkDto posixTime idNum activityString qualifierString =
    { activity = activityString
    , qualifier = qualifierString
    , time = Time.posixToMillis posixTime // 1000
    , idNum = idNum
    }


toDto : Activity -> ActivityDto
toDto act =
    case act of
        Feeding foodSource time idNum ->
            let
                mk =
                    mkDto time idNum "feed"
            in
            case foodSource of
                LeftBreast ->
                    mk "left"

                RightBreast ->
                    mk "right"

                Bottle ->
                    mk "bottle"

        Potty stool time idNum ->
            let
                mk =
                    mkDto time idNum "potty"
            in
            case stool of
                Pee ->
                    mk "pee"

                Poo ->
                    mk "poo"

                Both ->
                    mk "both"

        Unknown ->
            { activity = "unknown", qualifier = "unknown", time = -1, idNum = -1 }


toAct : ActivityDto -> Activity
toAct actResp =
    let
        posixTime =
            Time.millisToPosix (actResp.time * 1000)
    in
    case actResp.activity of
        "feed" ->
            case actResp.qualifier of
                "left" ->
                    Feeding LeftBreast posixTime actResp.idNum

                "right" ->
                    Feeding RightBreast posixTime actResp.idNum

                "bottle" ->
                    Feeding Bottle posixTime actResp.idNum

                _ ->
                    Unknown

        "potty" ->
            case actResp.qualifier of
                "poo" ->
                    Potty Poo posixTime actResp.idNum

                "pee" ->
                    Potty Pee posixTime actResp.idNum

                "both" ->
                    Potty Both posixTime actResp.idNum

                _ ->
                    Unknown

        _ ->
            Unknown


decodeStatsResponse : D.Decoder StatsDto
decodeStatsResponse =
    D.map3 StatsDto
        (D.field "average" D.int)
        (D.field "high" D.int)
        (D.field "low" D.int)


decodeActivityResponse : D.Decoder ActivityDto
decodeActivityResponse =
    D.map4 ActivityDto
        (D.field "activity" D.string)
        (D.field "qualifier" D.string)
        (D.field "time" D.int)
        (D.field "id" D.int)


decodeAckResponse : D.Decoder AckResponse
decodeAckResponse =
    D.map2 AckResponse
        (D.field "message" D.string)
        (D.field "success" D.bool)


decodeAuthResponse : D.Decoder AuthResponse
decodeAuthResponse =
    D.map4 AuthResponse
        (D.field "id" D.int)
        (D.field "accountId" D.int)
        (D.field "email" D.string)
        (D.field "name" D.string)


activityResponseDecoder : D.Decoder (List ActivityDto)
activityResponseDecoder =
    D.list decodeActivityResponse



---- MODEL ----


type UrlRoute
    = Root
    | SetPass String


urlRouteToResetToken : UrlRoute -> Maybe String
urlRouteToResetToken route =
    case route of
        Root ->
            Nothing

        SetPass token ->
            Just token


type ActivityHttp
    = Error String
    | Activities (List ActivityDto)


type FetchedActivity
    = Fetching
    | ErrorMsg String
    | Fetched (List Activity)


type FetchedStats
    = StatsFetching
    | StatsError String
    | StatsFetched StatsDto


type alias Login =
    { email : String
    , password : String
    }


type alias User =
    { id : Int
    , email : String
    , accountId : Int
    , name : String
    }


type TrakrSession
    = Anon
    | LoggedIn User


type alias Model =
    { api_url : String
    , booting : Bool
    , activities : FetchedActivity
    , stats : FetchedStats
    , statsModalOpen : Bool
    , currentTimeZone : Maybe Zone
    , newActivityModalOpen : Bool
    , editActivityModalOpen : Bool
    , modalActivity : Activity -- placeholder while new activity is POSTed to server
    , login : Login
    , session : TrakrSession
    , resetToken : Maybe String
    , resetError : Maybe String
    , newPassword : String
    , confirmNewPassword : String
    , resetCode : String
    }


type alias Config =
    { api_base_url : String }


getTime : Cmd Msg
getTime =
    Task.perform TimeReceived Time.here


init : String -> Maybe String -> ( Model, Cmd Msg )
init url token =
    ( { api_url = url
      , booting = True
      , stats = StatsFetching
      , statsModalOpen = False
      , activities = Fetching
      , currentTimeZone = Nothing
      , newActivityModalOpen = False
      , editActivityModalOpen = False
      , modalActivity = defaultActivity
      , login = { email = "", password = "" }
      , session = Anon
      , resetToken = token
      , resetError = Nothing
      , newPassword = ""
      , confirmNewPassword = ""
      , resetCode = ""
      }
    , Cmd.batch [ checkSession url, getTime ]
    )



---- UPDATE ----


type Msg
    = NoOp
    | Done
    | StatsReceived (Result Http.Error StatsDto)
    | CheckReceived (Result Http.Error AuthResponse)
    | AskForStats
    | DismissStats
    | ActivitiesReceived (Result Http.Error (List ActivityDto))
    | TimeReceived Zone
    | NewActivityOpen
    | NewActivityClose
    | NewActivity
    | NewActivitySaved (Result Http.Error ActivityDto)
    | EditActivityOpen Activity
    | EditActivityClose
    | ActivityEditted
    | EditActivitySaved (Result Http.Error ActivityDto)
    | RemoveActivity Activity
    | ActivityRemoved (Result Http.Error AckResponse)
    | ToggleModalActFeed
    | ToggleModalActPotty
    | ToggleModalActPoo
    | ToggleModalActPee
    | ToggleModalActLeft
    | ToggleModalActRight
    | ToggleModalActBottle
    | SetEmail String
    | SetPassword String
    | TryLogin
    | LoginReceived (Result Http.Error AuthResponse)
    | ResettingPass String
    | TrySetNewPassword
    | SetNewPassword String
    | SetResetCode String
    | SetConfirmPassword String
    | ResetResponseReceived (Result Http.Error AuthResponse)


withNoCmd : Model -> ( Model, Cmd Msg )
withNoCmd model =
    ( model, Cmd.none )


modelWithModalAct : Model -> Activity -> ( Model, Cmd Msg )
modelWithModalAct model act =
    withNoCmd { model | modalActivity = act }


getErrStr : Http.Error -> String
getErrStr err =
    case err of
        Http.BadUrl _ ->
            "Bad Url"

        Http.Timeout ->
            "Request timed out"

        Http.NetworkError ->
            "Network error"

        Http.BadStatus _ ->
            "Server returned a bad status"

        Http.BadBody _ ->
            "We sent a bad request"


onSessionExpired : Model -> ( Model, Cmd Msg )
onSessionExpired model =
    withNoCmd { model | login = { email = "", password = "" }, session = Anon }


handleHttpErr : Model -> Http.Error -> (String -> ( Model, Cmd Msg )) -> ( Model, Cmd Msg )
handleHttpErr model err withErrMsg =
    case err of
        Http.BadStatus 401 ->
            onSessionExpired model

        _ ->
            getErrStr err
                |> withErrMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            withNoCmd model

        Done ->
            withNoCmd { model | booting = False }

        AskForStats ->
            ( model, getStats model.api_url )

        SetEmail em ->
            withNoCmd { model | login = { email = em, password = model.login.password } }

        SetPassword pass ->
            withNoCmd { model | login = { password = pass, email = model.login.email } }

        TryLogin ->
            ( { model | booting = True }, loginPost model.api_url model.login )

        LoginReceived res ->
            case res of
                Ok authResp ->
                    -- if ack.success then
                    ( { model
                        | booting = False
                        , login = { email = "", password = "" }
                        , session = LoggedIn { email = model.login.email, id = authResp.id, accountId = authResp.accountId, name = authResp.name }
                      }
                    , getActivities model.api_url
                    )

                -- else
                -- TODO show bad login
                -- ( { model | booting = False, login = { email = model.login.email, password = "" } }, Cmd.none )
                Err err ->
                    -- TODO have someway to handle 400 json
                    handleHttpErr model
                        err
                        (\errMsg ->
                            withNoCmd { model | booting = False, stats = StatsError errMsg }
                        )

        CheckReceived res ->
            case res of
                Ok authResp ->
                    ( { model
                        | booting = False
                        , login = { email = "", password = "" }
                        , session = LoggedIn { email = model.login.email, id = authResp.id, accountId = authResp.accountId, name = authResp.name }
                      }
                    , getActivities model.api_url
                    )

                Err err ->
                    handleHttpErr model
                        err
                        (\errMsg ->
                            withNoCmd { model | booting = False, session = Anon }
                        )

        DismissStats ->
            withNoCmd { model | statsModalOpen = False }

        StatsReceived res ->
            case res of
                Ok data ->
                    withNoCmd { model | statsModalOpen = True, stats = data |> StatsFetched }

                Err err ->
                    handleHttpErr model
                        err
                        (\errMsg ->
                            withNoCmd { model | stats = StatsError errMsg }
                        )

        ActivitiesReceived res ->
            case res of
                Ok data ->
                    withNoCmd { model | activities = List.map toAct data |> Fetched }

                Err err ->
                    handleHttpErr model
                        err
                        (\errMsg ->
                            withNoCmd { model | activities = ErrorMsg errMsg }
                        )

        TimeReceived zone ->
            withNoCmd { model | currentTimeZone = Just zone }

        NewActivityOpen ->
            withNoCmd { model | newActivityModalOpen = True }

        NewActivityClose ->
            withNoCmd { model | newActivityModalOpen = False, modalActivity = defaultActivity }

        NewActivity ->
            ( model, postNewActivity model.api_url model.modalActivity )

        NewActivitySaved saveResult ->
            case saveResult of
                Ok newDto ->
                    { model
                        | modalActivity = defaultActivity
                        , newActivityModalOpen = False
                        , activities =
                            case model.activities of
                                Fetched acts ->
                                    (toAct newDto :: acts) |> Fetched

                                _ ->
                                    model.activities
                    }
                        |> withNoCmd

                Err err ->
                    handleHttpErr model
                        err
                        (\errMsg ->
                            -- TODO display err msg?
                            ( model, Cmd.none )
                        )

        ToggleModalActFeed ->
            model.modalActivity
                |> toFeed
                |> modelWithModalAct model

        ToggleModalActPotty ->
            model.modalActivity
                |> toPotty
                |> modelWithModalAct model

        ToggleModalActPoo ->
            model.modalActivity
                |> togglePoo
                |> modelWithModalAct model

        ToggleModalActPee ->
            model.modalActivity
                |> togglePee
                |> modelWithModalAct model

        ToggleModalActLeft ->
            model.modalActivity
                |> toLeft
                |> modelWithModalAct model

        ToggleModalActRight ->
            model.modalActivity
                |> toRight
                |> modelWithModalAct model

        ToggleModalActBottle ->
            model.modalActivity
                |> toBottle
                |> modelWithModalAct model

        RemoveActivity act ->
            -- TODO maybe add loading state?
            ( model, deleteActivity model.api_url (getId act) )

        ActivityRemoved resp ->
            case resp of
                Ok ack ->
                    { model
                        | modalActivity = defaultActivity
                        , editActivityModalOpen = False
                        , activities =
                            case model.activities of
                                Fetched acts ->
                                    acts
                                        |> List.filter (\act -> not (getId act == getId model.modalActivity))
                                        |> Fetched

                                _ ->
                                    model.activities
                    }
                        |> withNoCmd

                Err err ->
                    handleHttpErr model
                        err
                        (\errMsg ->
                            -- TODO display err msg?
                            ( model, Cmd.none )
                        )

        EditActivityOpen act ->
            withNoCmd { model | modalActivity = act, editActivityModalOpen = True }

        EditActivityClose ->
            withNoCmd { model | modalActivity = defaultActivity, editActivityModalOpen = False }

        ActivityEditted ->
            -- TODO maybe add loading here?
            ( model, updateActivity model.api_url model.modalActivity )

        EditActivitySaved editRes ->
            case editRes of
                Ok edittedAct ->
                    { model
                        | modalActivity = defaultActivity
                        , editActivityModalOpen = False
                        , activities =
                            case model.activities of
                                Fetched acts ->
                                    acts
                                        |> List.map
                                            (\a ->
                                                let
                                                    id =
                                                        getId a
                                                in
                                                if id == edittedAct.idNum then
                                                    toAct edittedAct

                                                else
                                                    a
                                            )
                                        |> Fetched

                                _ ->
                                    model.activities
                    }
                        |> withNoCmd

                Err err ->
                    handleHttpErr model
                        err
                        (\errMsg ->
                            -- TODO display err msg?
                            ( model, Cmd.none )
                        )

        SetNewPassword newPass ->
            { model | newPassword = newPass, resetError = Nothing }
                |> withNoCmd

        SetConfirmPassword confirmedNewPass ->
            { model | confirmNewPassword = confirmedNewPass, resetError = Nothing }
                |> withNoCmd

        SetResetCode resetCode ->
            { model | resetCode = resetCode }
                |> withNoCmd

        TrySetNewPassword ->
            if (String.isEmpty >> not) model.newPassword && model.newPassword == model.confirmNewPassword then
                ( { model | booting = True }
                , resetPost
                    model.api_url
                    { token = Maybe.withDefault "" model.resetToken
                    , newPass = model.newPassword
                    , resetCode = model.resetCode
                    }
                )

            else
                { model
                    | resetError =
                        Just <|
                            if String.isEmpty model.newPassword then
                                "Password cannot be empty."

                            else
                                "Passwords did not match."
                    , newPassword = ""
                    , confirmNewPassword = ""
                }
                    |> withNoCmd

        ResettingPass token ->
            { model | resetToken = Just token }
                |> withNoCmd

        ResetResponseReceived res ->
            case res of
                Ok authResp ->
                    ( { model
                        | booting = False
                        , resetToken = Nothing
                        , newPassword = ""
                        , confirmNewPassword = ""
                        , resetCode = ""
                        , resetError = Nothing
                        , session = LoggedIn { email = model.login.email, id = authResp.id, accountId = authResp.accountId, name = authResp.name }
                      }
                    , load (absolute [] [])
                      --, getActivities model.api_url
                    )

                Err err ->
                    -- TODO handle errors here?
                    handleHttpErr model
                        err
                        (\errMsg ->
                            withNoCmd { model | booting = False, resetError = Just <| "Reset request failed: " ++ errMsg }
                        )



---- VIEW ----


getMonthStr : Month -> String
getMonthStr month =
    case month of
        Time.Jan ->
            "Jan"

        Time.Feb ->
            "Feb"

        Time.Mar ->
            "Mar"

        Time.Apr ->
            "Apr"

        Time.May ->
            "May"

        Time.Jun ->
            "Jun"

        Time.Jul ->
            "Jul"

        Time.Aug ->
            "Aug"

        Time.Sep ->
            "Sep"

        Time.Oct ->
            "Oct"

        Time.Nov ->
            "Nov"

        Time.Dec ->
            "Dec"


getWeekdayStr : Weekday -> String
getWeekdayStr wkd =
    case wkd of
        Time.Mon ->
            "Mon"

        Time.Tue ->
            "Tue"

        Time.Wed ->
            "Wed"

        Time.Thu ->
            "Thu"

        Time.Fri ->
            "Fri"

        Time.Sat ->
            "Sat"

        Time.Sun ->
            "Sun"


toReadable : Maybe Zone -> Posix -> String
toReadable z time =
    case z of
        Just zone ->
            let
                month =
                    Time.toMonth zone time
                        |> getMonthStr

                day =
                    Time.toDay zone time
                        |> String.fromInt

                weekday =
                    Time.toWeekday zone time
                        |> getWeekdayStr

                -- TODO support 12hr format too
                hour =
                    Time.toHour zone time
                        |> String.fromInt

                min =
                    Time.toMinute zone time
                        |> String.fromInt
                        |> String.pad 2 '0'
            in
            String.concat [ hour, ":", min, " on ", weekday, ", ", month, " ", day ]

        Nothing ->
            "... (waiting on time zone info...)"


makeActivityClickHandler : Activity -> Html.Attribute Msg
makeActivityClickHandler act =
    onClick <| EditActivityOpen act


renderActivity : Maybe Zone -> Activity -> Html Msg
renderActivity zone act =
    let
        formatTime =
            toReadable zone

        liAct txt =
            li [ makeActivityClickHandler act ] [ text txt ]
    in
    liAct
        (case act of
            Feeding side time _ ->
                case side of
                    LeftBreast ->
                        "🍼 Baby fed on the left breast at " ++ formatTime time

                    RightBreast ->
                        "🍼 Baby fed on the right breast at " ++ formatTime time

                    Bottle ->
                        "🍼 Baby fed from a bottle at " ++ formatTime time

            Potty stool time _ ->
                case stool of
                    Pee ->
                        "💧 Baby peed at " ++ formatTime time

                    Poo ->
                        "💩 Baby pooped at " ++ formatTime time

                    Both ->
                        "😲 Baby pooped and peed at " ++ formatTime time

            Unknown ->
                " -- ? Unknown activity ? --"
        )


view : Model -> Browser.Document Msg
view model =
    { title = "Trakr"
    , body =
        if model.booting then
            [ div [ class "app" ] [ div [ class "loading-spinner" ] [] ] ]

        else
            case model.resetToken of
                Nothing ->
                    [ case model.session of
                        Anon ->
                            div
                                [ class "app"
                                ]
                                [ h1 [] [ text "Trakr" ]
                                , h3 [] [ text "Please login to access your baby or babies feed(s)!" ]
                                , p [] [ text "Logins will last 7 days at a time." ]
                                , form [ onSubmit TryLogin ]
                                    [ fieldset [ class "login" ]
                                        [ label [] [ text "Email" ]
                                        , input [ type_ "text", onInput SetEmail ] []
                                        ]
                                    , fieldset [ class "login" ]
                                        [ label [] [ text "Password" ]
                                        , input [ type_ "password", onInput SetPassword ] []
                                        ]
                                    , button
                                        [ class "primary-button"
                                        , class "accent-bg"
                                        , type_ "submit"
                                        , style "font-size" "1.4em"
                                        , style "margin-top" ".4em"
                                        , onSubmit TryLogin
                                        ]
                                        [ text "Login" ]
                                    ]
                                ]

                        LoggedIn user ->
                            renderApp user model
                    ]

                Just token ->
                    [ div
                        [ class "app" ]
                        [ h1 [] [ text "Trakr" ]
                        , h3 [] [ text "Resetting Password" ]
                        , p [] [ text "This reset feature (currently) is only a one-time setup - remember your password!" ]
                        , form [ onSubmit TrySetNewPassword ]
                            [ fieldset [ class "login" ]
                                [ label [] [ text "Reset Code - Get this from Sean" ]
                                , input [ type_ "password", onInput SetResetCode ] []
                                ]
                            , fieldset [ class "login" ]
                                [ label [] [ text "New Password" ]
                                , input [ type_ "password", onInput SetNewPassword ] []
                                ]
                            , fieldset [ class "login" ]
                                [ label [] [ text "Confirm Password" ]
                                , input [ type_ "password", onInput SetConfirmPassword ] []
                                ]
                            , case model.resetError of
                                Nothing ->
                                    span [] []

                                Just errStr ->
                                    fieldset [ class "login" ] [ label [ class "error" ] [ text errStr ] ]
                            , button
                                [ class "primary-button"
                                , class "accent-bg"
                                , type_ "submit"
                                , style "font-size" "1.4em"
                                , style "margin-top" ".4em"
                                ]
                                [ text "Reset Password" ]
                            ]
                        ]
                    ]
    }


renderApp : User -> Model -> Html Msg
renderApp user model =
    div
        [ class "app"
        , class
            (if model.newActivityModalOpen then
                "scroll-block"

             else
                ""
            )
        ]
        [ h1 [] [ text "Activities" ]
        , div []
            [ button [ class "primary-button", class "accent-bg", onClick NewActivityOpen ]
                [ b [] [ text "+" ]
                , span [] [ text " New Activity" ]
                ]
            , button [ class "secondary-button", class "stats", onClick AskForStats ]
                [ span [] [ text "Show Stats" ] ]
            ]
        , case model.activities of
            Fetched acts ->
                if List.isEmpty acts then
                    h2 []
                        [ span [] [ text "Looks like you don't have any activities yet, start " ]
                        , i [] [ text "traking" ]
                        , span [] [ text " and get cracking!" ]
                        ]

                else
                    acts
                        |> List.map (renderActivity model.currentTimeZone)
                        |> ul [ class "activities" ]

            Fetching ->
                div [ class "loading-spinner" ] []

            ErrorMsg msg ->
                div [] [ text msg ]
        , if model.statsModalOpen then
            case model.stats of
                StatsFetched stats ->
                    renderStatsModal stats

                _ ->
                    div [] []

          else if model.newActivityModalOpen then
            renderActivityModal True model.modalActivity

          else if model.editActivityModalOpen then
            renderActivityModal False model.modalActivity

          else
            div [] []
        ]


fieldSetLabel : String -> Html Msg
fieldSetLabel txt =
    p [ style "font-size" "1.2em", style "text-align" "left" ] [ text txt ]


renderStatsModal : StatsDto -> Html Msg
renderStatsModal stats =
    div [ class "dialog-container" ]
        [ div [ class "dialog-content" ]
            [ h1
                []
                [ text "Stats" ]
            , h3
                []
                [ text "Average number of wet diapers over the last week." ]
            , p
                []
                [ text "Activities are only counted after a full day (midnight) has passed." ]
            , ul [ class "activities" ]
                [ li []
                    [ b [] [ text "Average: " ]
                    , stats.average |> String.fromInt |> text
                    ]
                , li []
                    [ b [] [ text "High: " ]
                    , stats.high |> String.fromInt |> text
                    ]
                , li []
                    [ b [] [ text "Low: " ]
                    , stats.low |> String.fromInt |> text
                    ]
                ]
            , div [ class "dialog-buttons" ]
                [ button
                    [ style "font-size" "1.3em", class "primary-button", class "accent-bg", onClick DismissStats ]
                    [ text "Okay!" ]
                ]
            ]
        ]


renderActivityModal : Bool -> Activity -> Html Msg
renderActivityModal isNew act =
    div [ class "dialog-container" ]
        [ div [ class "dialog-content" ]
            [ p
                [ style "font-size" "1.4em", style "line-height" "1.3" ]
                [ text
                    (if isNew then
                        "Create a new activity! This will get saved and pushed to the front of the activity list"

                     else
                        "Edit your activity ✍️"
                    )
                ]
            , fieldSetLabel "Type"
            , fieldset [ class "spacious" ]
                -- TODO fix the 'gap between the circle label and the text
                [ input [ onClick ToggleModalActFeed, id "new-act-feed", name "new-act-feed", class "fun-radio", type_ "checkbox", checked <| isFeed act ] []
                , label [ class "fun-radio-label", for "new-act-feed" ] [ text "Feed" ]
                , input [ onClick ToggleModalActPotty, id "new-act-potty", name "new-act-potty", class "fun-radio", type_ "checkbox", checked <| isPotty act ] []
                , label [ class "fun-radio-label", for "new-act-potty" ] [ text "Potty" ]
                ]
            , fieldSetLabel
                (if isFeed act then
                    "What kind of feeding?"

                 else
                    "#1, #2, or both?"
                )
            , fieldset [ class "spacious" ]
                (if isFeed act then
                    [ input [ onClick ToggleModalActLeft, id "new-act-left", name "new-act-left", class "fun-radio", type_ "checkbox", checked <| isLeft act ] []
                    , label [ class "fun-radio-label", for "new-act-left" ] [ text "Left" ]
                    , input [ onClick ToggleModalActRight, id "new-act-right", name "new-act-right", class "fun-radio", type_ "checkbox", checked <| isRight act ] []
                    , label [ class "fun-radio-label", for "new-act-right" ] [ text "Right" ]
                    , input [ onClick ToggleModalActBottle, id "new-act-bottle", name "new-act-bottle", class "fun-radio", type_ "checkbox", checked <| isBottle act ] []
                    , label [ class "fun-radio-label", for "new-act-bottle" ] [ text "Bottle" ]
                    ]

                 else
                    [ input [ onClick ToggleModalActPee, id "new-act-pee", name "new-act-pee", class "fun-radio", type_ "checkbox", checked <| isPee act ] []
                    , label [ class "fun-radio-label", for "new-act-pee" ] [ text "Pee" ]
                    , input [ onClick ToggleModalActPoo, id "new-act-poo", name "new-act-poo", class "fun-radio", type_ "checkbox", checked <| isPoo act ] []
                    , label [ class "fun-radio-label", for "new-act-poo" ] [ text "Poo" ]
                    ]
                )
            , div [ class "dialog-buttons" ]
                (if isNew then
                    renderNewModalButtons act

                 else
                    renderEditModalButtons act
                )
            ]
        ]


renderNewModalButtons : Activity -> List (Html Msg)
renderNewModalButtons act =
    [ button [ style "font-size" "1.3em", class "primary-button", class "accent-bg", onClick <| NewActivity ] [ text "Submit" ]
    , button
        [ style "margin-left" "1.4em"
        , style "font-size" "1.1em"
        , class "secondary-button"
        , onClick NewActivityClose
        ]
        [ text "Cancel" ]
    ]


renderEditModalButtons : Activity -> List (Html Msg)
renderEditModalButtons act =
    [ button
        [ style "font-size" "1.3em"
        , class "primary-button"
        , class "accent-bg"
        , onClick ActivityEditted
        ]
        [ text "Save Edits" ]
    , button
        [ class "secondary-button"
        , style "font-size" "1.1em"
        , style "margin-left" "1.4em"
        , onClick <| RemoveActivity act
        ]
        [ text "Delete" ]
    , button
        [ style "margin-left" "1.4em"
        , style "font-size" "1.1em"
        , class "secondary-button"
        , onClick EditActivityClose
        ]
        [ text "Cancel" ]
    ]



---- HTTP ----


checkSession : String -> Cmd Msg
checkSession basePath =
    Api.get
        (Http.expectJson CheckReceived decodeAuthResponse)
        (basePath ++ "/check")


loginPost : String -> Login -> Cmd Msg
loginPost basePath loginCreds =
    Api.post
        (loginToJson loginCreds)
        (Http.expectJson LoginReceived decodeAuthResponse)
        (basePath ++ "/login")


resetPost : String -> ResetPassDto -> Cmd Msg
resetPost basePath resetCreds =
    Api.post
        (resetToJson resetCreds)
        (Http.expectJson ResetResponseReceived decodeAuthResponse)
        (basePath ++ "/reset")


getStats : String -> Cmd Msg
getStats basePath =
    Api.get
        (Http.expectJson StatsReceived decodeStatsResponse)
        (basePath ++ "/stats")


getActivities : String -> Cmd Msg
getActivities basePath =
    Api.get
        (Http.expectJson ActivitiesReceived activityResponseDecoder)
        (basePath ++ "/activities")


postNewActivity : String -> Activity -> Cmd Msg
postNewActivity basePath act =
    Api.post
        (activityToJson act)
        (Http.expectJson NewActivitySaved decodeActivityResponse)
        (basePath ++ "/activities")


updateActivity : String -> Activity -> Cmd Msg
updateActivity basePath act =
    Api.patch
        (activityToJson act)
        (Http.expectJson EditActivitySaved decodeActivityResponse)
        (basePath ++ "/activities/" ++ String.fromInt (getId act))


deleteActivity : String -> Int -> Cmd Msg
deleteActivity basePath actId =
    Api.delete
        (Http.expectJson ActivityRemoved decodeAckResponse)
        (basePath ++ "/activities/" ++ String.fromInt actId)


activityToJson : Activity -> Http.Body
activityToJson =
    toDto >> encodeAct >> Http.jsonBody


loginToJson : Login -> Http.Body
loginToJson =
    encodeLogin >> Http.jsonBody


encodeLogin : Login -> E.Value
encodeLogin creds =
    E.object
        [ ( "email", E.string creds.email )
        , ( "password", E.string creds.password )
        ]


resetToJson : ResetPassDto -> Http.Body
resetToJson =
    encodeResetPass >> Http.jsonBody


encodeResetPass : ResetPassDto -> E.Value
encodeResetPass resetReq =
    E.object
        [ ( "token", E.string resetReq.token )
        , ( "newPass", E.string resetReq.newPass )
        , ( "resetCode", E.string resetReq.resetCode )
        ]


encodeAct : ActivityDto -> E.Value
encodeAct act =
    E.object
        [ ( "activity", E.string act.activity )
        , ( "qualifier", E.string act.qualifier )
        , ( "time", E.int act.time )
        ]


maybeResetToken : Maybe String -> UrlRoute
maybeResetToken ms =
    case ms of
        Nothing ->
            Root

        Just frag ->
            if String.isEmpty frag then
                Root

            else
                SetPass frag


routeParser : Parser (UrlRoute -> a) a
routeParser =
    oneOf
        [ Url.Parser.map Root top
        , Url.Parser.s "reset" </> fragment maybeResetToken
        ]


toRoute : Url.Url -> UrlRoute
toRoute url =
    parse routeParser url
        |> Maybe.withDefault Root



---- PROGRAM ----


main : Program Config Model Msg
main =
    Browser.application
        { view = view
        , init =
            \cfg browserUrl key ->
                toRoute browserUrl
                    |> urlRouteToResetToken
                    |> init cfg.api_base_url
        , onUrlRequest =
            \requestUrl ->
                case requestUrl of
                    Browser.Internal url ->
                        case toRoute url of
                            Root ->
                                Done

                            SetPass token ->
                                ResettingPass token

                    Browser.External _ ->
                        Done
        , onUrlChange =
            \newUrl ->
                case toRoute newUrl of
                    Root ->
                        Done

                    SetPass token ->
                        ResettingPass token
        , update = update
        , subscriptions = always Sub.none
        }
