#!/bin/bash

echo 'starting env var replacement...'

# This is the defined entrypoint for the docker nginx host
# We get the CMD to start the daemon passed in, so we must end with
# execing that passed in CMD from docker

apiBaseUrl=${ELM_APP_API_BASE_URL-http://localhost:8080}
jsPath=/app/static/js/*.js

function findAndReplace () {
    grep -l $1 $jsPath | xargs sed -i "s|$1|$2|g"
}

echo 'Replacing api base url...'
# ensure all replaceable env vars are setup here
findAndReplace "%%API_BASE_URL%%" $apiBaseUrl 

echo 'Reapplying file permissions...'
chmod -R 0755 /app

echo 'Starting nginx...'
# start nginx, non-daemonized
exec $(which nginx) -c /etc/nginx/nginx.conf -g "daemon off;"

