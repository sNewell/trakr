module Config

open FsConfig

/// Trakr configuration (env vars or a json file)
type Config = {
  [<DefaultValue("localhost")>]
  DbHost : string
  [<DefaultValue("54320")>]
  DbPort : int
  [<DefaultValue("postgres")>]
  DbSchema : string
  [<DefaultValue("trakrapp")>]
  DbUser : string
  [<DefaultValue("pass")>]
  DbPass : string
  [<DefaultValue("8080")>]
  Port : int
  [<DefaultValue("8081")>]
  SecurePort : int
  [<DefaultValue("127.0.0.1")>]
  IP : string
  // generate a key with Suave.Utils (see https://suave.io/sessions.html)
  [<DefaultValue("gGhdUL+q0tqsI0j/e3LBw8pFLS5usoMoY3bY1TeuAyI=")>]
  ServerCookieKey : string
  [<DefaultValue("notaverysecurekey")>]
  SecurityKey : string
  [<DefaultValue("thisisnotagoodinvitecode")>]
  /// Special code to register accounts with
  InviteCode : string
  [<DefaultValue("testing")>]
  ResetCode : string
  [<DefaultValue("sandbox418d640d27b7438cbd57f69b56f1770f.mailgun.org")>]
  MailgunDomain : string
  [<DefaultValue("app")>]
  MailgunFromUser : string
  MailgunApiKey : string
  [<DefaultValue("http://localhost:8080")>]
  TrakrHost : string
}


let configErrToStr (parseError: ConfigParseError): string =
  match parseError with
  | BadValue (prop, value) ->
    sprintf "Config error: %s could not be used with value %s." prop value
  | NotFound prop ->
    sprintf "Config error: %s could not be found." prop
  | NotSupported prop ->
    sprintf "Config error: %s - no way man." prop

let topLevelConfig =
  EnvConfig.Get<Config>()

let withConfig f =
  topLevelConfig
  |> Result.map f
  |> Result.mapError configErrToStr

let configToConnStr cfg =
  sprintf "Host=%s;Port=%d;Database=%s;Username=%s;Password=%s"
    cfg.DbHost
    cfg.DbPort
    cfg.DbSchema
    cfg.DbUser
    cfg.DbPass

