module Email

open FSharp.Data
open Config
open EmailTemplates

let private mailGunRoot = "https://api.mailgun.net/v3/"

let private mkEmailAddress fromUser domain =
  sprintf "%s <%s@%s>" fromUser fromUser domain

let private mkMessageUrl baseUrl domain =
  sprintf "%s%s/messages" baseUrl domain

let private mkMailgunAuth key = sprintf "%s:%s" "api" key
let private mkEncodedMailgunAuth key = System.Text.Encoding.UTF8.GetBytes(mkMailgunAuth key) |> System.Convert.ToBase64String
let private mkMailgunAuthHeaders key = [ ( "Authorization", "Basic " + (mkEncodedMailgunAuth key) ) ]

let private mkMailgunMessageUrl domain = mkMessageUrl mailGunRoot domain

/// Sends an application email from trakr with the subject and body as a plain text email
let sendMailPlain recip subject body =
  withConfig (fun cfg ->
    let url = mkMailgunMessageUrl cfg.MailgunDomain
    let fromAddress = mkEmailAddress cfg.MailgunFromUser cfg.MailgunDomain
    let authHeaders = mkMailgunAuthHeaders cfg.MailgunApiKey

    Http.RequestString
     ( url,
      body =
        FormValues [
          ("from", fromAddress)
          ("to", recip)
          ("subject", subject)
          ("text", body)
        ],
      headers = authHeaders
    )
  )

let sendMailHtml recip subject htmlBod =
  withConfig (fun cfg ->
    let url = mkMailgunMessageUrl cfg.MailgunDomain
    let fromAddress = mkEmailAddress cfg.MailgunFromUser cfg.MailgunDomain
    let authHeaders = mkMailgunAuthHeaders cfg.MailgunApiKey

    Http.RequestString
     ( url,
      body =
        FormValues [
          ("from", fromAddress)
          ("to", recip)
          ("subject", subject)
          ("html", htmlBod)
        ],
      headers = authHeaders
    )
  )

let sendPasswordResetEmail accName link recip subject =
  mkPasswordResetEmail accName link
  |> sendMailHtml recip subject
