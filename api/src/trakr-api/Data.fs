/// Data access functions
module Data

open SqlStrs
open Auth
open Dapper
open Npgsql
open Model
open Config
open Time

let private withConn f connStr =
  let conn = new NpgsqlConnection(connStr)
  conn.Open()
  let toRet = f conn
  conn.Dispose()
  toRet

let private withConnRes f connStr =
  try
    withConn f connStr
    |> Ok
  with Ex ->
    Ex.Message
    |> sprintf "DB Error: %s"
    |> Error

/// Helper function to grab config, get db connection string, and get the db connection.
/// Your 'f' can fail, and this function will put the exception
/// message into a Result Error
let private withDb f =
  withConfig configToConnStr
  |> Result.bind (withConnRes f)


type AccountId = {
  accountId : int
}

type OnlyId = {
  id : int
}

let readActitiesFromDb accId =
  withDb (fun db ->
    db.Query<ActivityEntity>(LastFiftyActivitiesSql, { accountId = accId })
    |> Seq.map (fun ae ->
      match actFromEntity ae with
      | Ok act -> act
      | Error msg -> failwith msg) // TODO our db should _never_ have bad data in it.... but you neverk now! Propogate this error to provide better error msg to consumers of this API
  )

let readActivityFromDb actId =
  withDb (fun db ->
    db.QueryFirst<ActivityEntity>(ActivityByIdSql, { id = actId })
  )
  |> Result.bind actFromEntity

let saveActToDb accId act  =
  withDb (fun db ->
    let data = actToEntity accId act
    let id =
      db.ExecuteScalar(InsertActivitySql, data)
      :?> int
    if id > 0 then
      withId id act
    else
      failwith "Failed to insert..."
  )
  |> Result.mapError (fun _ -> "error when inserting")

let updateDb accId act =
  withDb (fun db ->
    let data = actToEntity accId act
    let rowsUpdated = db.Execute(UpdateActivitySql, data)
    if rowsUpdated = 1 then
      act
    else
      failwith "Failed to update"
  )

type StatsData = {
  accountId : int
  fromutc : int64
  toutc : int64
}

// gathers daily average (with high/lo) from passed in timestamp to now
let readStatsDb accId fromDateUtc =
  withDb (fun db ->
    let sqlParams = { accountId = accId; fromutc = fromDateUtc; toutc = getRoundedToday () }

    let stats = db.QueryFirst<StatsEntity>(StatsSql, sqlParams)

    {
      average = stats.average |> int;
      high = stats.max |> int;
      low = stats.min |> int;
    }
  )

let removeActivityById id =
  withDb (fun db ->
    let rowsDeleted = db.Execute(DeleteActivitySql, { id = id })
    if rowsDeleted = 1 then
      true
    else
      failwithf "Failed to delete id %d" id
  )

type EmailParam = {
  email : string
}

let getUserByEmail (email: string) =
  withDb (fun db ->
    db.Query<UserEntity>(FetchPassHashByEmailSql, { email = email })
    |> Seq.head
  )

let getUserEntityFromEmailInfo (emailInfo: ResetEmailVerify) =
  withDb (fun db ->
    db.Query<UserEntity>(FetchUserEntityByUserId, { id = emailInfo.userId })
    |> Seq.head
  )

type SetPassHash = {
  id : int
  newPassHash : string
}

let updateUserPassHash (userId: int) (newPassHash: string) =
  withDb (fun db ->
    let rowsUpdated = db.Execute(UpdateUserPassHash, { id = userId; newPassHash = newPassHash })

    if rowsUpdated = 1 then
      newPassHash
    else
      failwith "Failed to update password"
  )