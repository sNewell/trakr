/// Any and all time related functions 
module Time

open System

let toUtc (dateTimeOffset: DateTimeOffset) =
  dateTimeOffset.ToUnixTimeSeconds()

let getNow () =
  DateTimeOffset.UtcNow

/// Takes a date time offset and returns a date time offset that
/// is 'rounded down' to the previous day (ie the last midnight).
/// Truncates everything except the day.
let roundDateBackToLastMidnight (dateTimeOffset: DateTimeOffset) =
  let year = dateTimeOffset.Year
  let month = dateTimeOffset.Month
  let day = dateTimeOffset.Day
  DateTimeOffset(year, month, day, 0, 0, 0, 0, TimeSpan.Zero)

let getTwoWeeksAgo () =
  (getNow ()).Subtract(TimeSpan(14, 0, 0, 0))

let getRoundedToday () =
  getNow ()
  |> roundDateBackToLastMidnight
  |> toUtc

let getRoundedTwoWeeksAgo () =
  getTwoWeeksAgo ()
  |> roundDateBackToLastMidnight
  |> toUtc