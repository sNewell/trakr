open System
open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.RequestErrors
open Suave.Json
open Suave.CORS
open Suave.Writers
open System.Runtime.Serialization
open Config
open Model
open Auth
open Helpers
open Suave.State.CookieStateStore
open Time
open Data
open Result.Operators


type StatsRequest = { fromDateUtc : int64 }

[<DataContract>]
type AckResp =  {
  [<field: DataMember(Name="message")>]
    message : string
  [<field: DataMember(Name="success")>]
    success : bool
}

let ackResponse resp =
  if resp then
    { message = "acknowledged"; success = true }
  else
    { message = "failed"; success = false }

let respToUtf8Json<'T> (a: 'T) =
  toJson a
  |> System.Text.Encoding.UTF8.GetString

/// A JSON request
let jsonReq wp =
  request wp
  >=> setMimeType "application/json"

let authenticatedJsonReq f =
  request (withAuthenticatedUser f)
  >=> setMimeType "application/json"


let gatherStats accId fromDateUtc =
  readStatsDb accId fromDateUtc
  |> foldResult
    id
    (fun err ->
      printfn "Error when gathering stats: %s" err
      { average = -1; high = -1; low = -1; }
    )

let readActivities =
  readActitiesFromDb
  >> foldResult
    id
    (fun _ -> Seq.empty<Activity>)

let defaultActivitiesWeb =
  authenticatedJsonReq (fun _ uer ->
    uer
    >!> (fun user -> user.accountId)
    >!> readActivities
    >!> Seq.map prepActivityForJson
    >!> Seq.toArray
    >!> toJson
    >!> System.Text.Encoding.UTF8.GetString
    |> handleResult
  )

let findActivityWeb id =
  jsonReq (fun r ->
    id
    |> readActivityFromDb
    >!> actToUtf8Json
    |> handleResult
  )

let acceptActivityWeb: WebPart =
  authenticatedJsonReq (fun r uer ->
    uer
    |> Result.bind (fun user ->
      fromJson r.rawForm
      |> (fun (act: ActivityJson) -> { act with time = toUtc <| getNow() } )
      |> convertFromJson
      |> Result.bind (saveActToDb user.accountId)
    )
    |> Result.map actToUtf8Json 
    |> handleResult
  )

let acceptRegisterWeb: WebPart =
  jsonReq (fun r ->
    Ok "noop"
    |> handleResult
  )

let attemptLoginWeb: WebPart =
  statefulForSession
  >=> jsonReq (fun r ->
    fromJson<LoginJson> r.rawForm
    |> (fun loginJson ->
      loginJson.email
      |> getUserByEmail
      |> Result.map (fun usr -> (usr, loginJson.password))
    )
    |> Result.bind verifypass
    |> Result.bind genJwt
    |> foldResult
      (fun (ue, token) ->
        setJwtCookie token
        >=> (OK (userEntityToUtf8Json ue))
      )
      BAD_REQUEST
  )

let updateActivityWeb (id: int): WebPart =
  authenticatedJsonReq (fun r uer ->
    uer
    |> Result.bind (fun user ->
      r.rawForm
      |> fromJson
      |> (fun (act: ActivityJson) -> { act with id = id } )
      |> convertFromJson
      |> Result.bind (updateDb user.accountId)
    )
    |> Result.map actToUtf8Json
    |> handleResult
  )

let removeActivityWeb (id: int): WebPart =
  jsonReq (fun _ ->
    id
    |> removeActivityById
    |> Result.map ackResponse
    |> Result.map respToUtf8Json
    |> handleResult
  )

let statsWeb: WebPart =
  authenticatedJsonReq (fun r uer ->
    uer
    |> Result.map (fun user ->
      let getStats = gatherStats user.accountId
      let twoWeeksAgo = getRoundedTwoWeeksAgo ()

      match r.queryParam "from" with
      | Choice1Of2 theVal ->
        match System.Int64.TryParse(theVal) with
        | (true, utcTime) ->
          getStats utcTime
        | _ ->
          getStats twoWeeksAgo
      | _ ->
        getStats twoWeeksAgo
    )
    |> Result.map respToUtf8Json
    |> handleResult
  )

let testEmailWeb: WebPart =
  authenticatedJsonReq (fun _ctx uer ->
    uer
    |> Result.bind (fun ue ->
      Email.sendPasswordResetEmail ue.name "https://trakr.thenewells.us" ue.email "test"
    )
    |> handleResult
  )

let checkLogin: WebPart =
  authenticatedJsonReq (fun _ uer ->
    uer
    |> Result.map ueToJson
    |> Result.map respToUtf8Json
    |> handleResultBoth
  )

let attemptResetWeb: WebPart =
  statefulForSession
  >=> jsonReq (fun ctx ->
    ctx.rawForm
    |> fromJson
    |> (fun (resetReq: ResetRequestJson) ->
      withConfig (fun cfg ->
        (resetReq, cfg.ResetCode)
      )
    )
    |> Result.bind (fun (req, realResetCode) ->
      handleJwtDecoding req.token
      |> Result.bind (fun emailInfo ->
        verifyReset (realResetCode, req.resetCode)
        |> Result.map (fun _ -> (req, emailInfo))
      )
    )
    |> Result.bind (fun (req, emailInfo) ->
      getUserEntityFromEmailInfo emailInfo
      |> Result.map (fun ue -> (req, ue))
    )
    |> Result.bind (fun (req, ue) ->
      updateUserPassHash ue.id (hash ue req.newPass)
      |> Result.map (fun passhash ->
        { ue with passhash = passhash }
      )
    )
    |> Result.bind genJwt
    |> foldResult
      (fun (ue, token) ->
        setJwtCookie token
        >=> (OK (userEntityToUtf8Json ue))
      )
      BAD_REQUEST
  )

let corsConfig =
  { defaultCORSConfig with
      exposeHeaders = InclusiveOption.All;
      allowedMethods =
        InclusiveOption.Some [
          HttpMethod.GET;
          HttpMethod.POST;
          HttpMethod.DELETE;
          HttpMethod.PATCH;
        ];
  }

let corsHandler = cors corsConfig

let corsPreflight =
  OPTIONS
  >=> corsHandler
  >=> NO_CONTENT

/// CORS Enabled path
let cPath pathName =
  corsHandler >=> path pathName

/// CORRS enabled path scan
let cPathScan pathName pf =
  corsHandler >=> pathScan pathName pf

let appPart =
  choose [
    corsPreflight
    cPath "/email" >=> GET >=> testEmailWeb
    cPath "/activities" >=> choose [
      GET >=> defaultActivitiesWeb
      POST >=> acceptActivityWeb
    ]
    cPathScan "/activities/%d" (fun actId ->
      choose [
        GET >=> findActivityWeb actId
        PATCH >=> updateActivityWeb actId
        DELETE >=> removeActivityWeb actId
      ]
    )
    cPath "/stats" >=> GET >=> statsWeb
    cPath "/greeting" >=> GET >=> OK "Ahoy Sailor!"
    cPath "/register" >=> POST >=> acceptRegisterWeb
    cPath "/login" >=> POST >=> attemptLoginWeb
    cPath "/check" >=> GET >=> checkLogin
    cPath "/reset" >=> POST >=> attemptResetWeb
  ]

let loadEnvIfNeeded (args: string array) =
  if args.Length > 0 && IO.File.Exists args.[0] && System.IO.Path.GetExtension(args.[0]) = ".env" then
    IO.File.ReadAllLines args.[0]
    |> Array.map (fun l ->
      let parts = l.Split('=')
      if parts.Length <> 2 then
        printfn "Rejected line '%s'" l
        ()
      else
        Environment.SetEnvironmentVariable(parts.[0], parts.[1])
        ()
    )
    |> ignore
  else
    ()

[<EntryPoint>]
let main args =

  loadEnvIfNeeded args

  withConfig (fun cfg ->

    let suaveConfig = {
      defaultConfig with
        serverKey = ServerKey.fromBase64 cfg.ServerCookieKey;
        bindings =
          [ HttpBinding.createSimple HTTP cfg.IP cfg.Port;
            // TODO make sure creating this HTTPS protocol binding works
            HttpBinding.createSimple (HTTPS null) cfg.IP cfg.SecurePort
          ];
    }

    startWebServer suaveConfig appPart
  ) |> foldResult
    (fun _ -> 0)
    (fun err ->
      printf "%s" err
      1)
