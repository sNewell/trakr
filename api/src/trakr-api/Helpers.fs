module Helpers

open Suave.Writers
open Suave.Operators
open Suave.Successful
open Suave.RequestErrors
open Model
open System.Runtime.Serialization

let foldResult onSuccess onError res =
  match res with
  | Ok d -> onSuccess d
  | Error reason -> onError reason

let handleErr err =
  match err with
    | "Unauthenticated" ->
      UNAUTHORIZED "Session expired"
      >=> setHeader "WWW-Authenticate" "noop"
    | _ -> BAD_REQUEST err

let handleResult =
  foldResult OK handleErr

let handleAuthResult =
  foldResult
    (userEntityToUtf8Json >> OK)
    handleErr

let handleResultBoth =
  foldResult OK OK
