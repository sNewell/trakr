module Auth

open Config
open Model
open Helpers
open Suave
open Suave.State
open Suave.State.CookieStateStore
open Suave.Json
open Suave.Operators
open Suave.Writers
open Microsoft.Extensions.Options
open Microsoft.AspNetCore.Identity
open JWT.Builder
open JWT.Algorithms
open System
open System.Runtime.Serialization

let wrapOpts a =
  Options.Create a

let passwordSettings: PasswordHasherOptions =
  let opts = PasswordHasherOptions ()
  opts.IterationCount <- 10000
  opts.CompatibilityMode <- PasswordHasherCompatibilityMode.IdentityV3
  opts

let hasher =
  new PasswordHasher<UserEntity>(wrapOpts passwordSettings)

let hash user pass =
  hasher.HashPassword(user, pass)

let verifypass (user, incpass) =
  let res = hasher.VerifyHashedPassword(user, user.passhash, incpass)

  match res with
  | PasswordVerificationResult.Success ->
    Ok user
  | PasswordVerificationResult.SuccessRehashNeeded ->
    printf "Password for userid %d email %s should be rehashed" user.id user.email
    Ok user
  | PasswordVerificationResult.Failed ->
    Error "Invalid Password"
  | _ ->
    printf "An invalid code was returned from VerifyHashedPassword: %d" (res |> int)
    Error "Invalid Password"

let genJwt (user: UserEntity): Result<UserEntity * string, string> =
  withConfig (fun cfg ->
    ( user
    , JwtBuilder()
      .WithAlgorithm(HMACSHA256Algorithm())
      .WithSecret([| cfg.SecurityKey |])
      .AddClaim("exp", DateTimeOffset.UtcNow.AddDays(7|>float).ToUnixTimeSeconds())
      .AddClaim("id", user.id)
      .AddClaim("email", user.email)
      .AddClaim("name", user.name)
      .AddClaim("accountId", user.accountId)
      .Build()
    )
  )

[<DataContract>]
type JwtClaim = {
  [<field: DataMember(Name="exp")>]
  exp : int64
  [<field: DataMember(Name="id")>]
  id : int
  [<field: DataMember(Name="email")>]
  email : string
  [<field: DataMember(Name="name")>]
  name : string
  [<field: DataMember(Name="accountId")>]
  accountId : int
}

[<DataContract>]
type ResetClaim = {
  [<field: DataMember(Name="exp")>]
  exp : int64
  [<field: DataMember(Name="email")>]
  email : string
  [<field: DataMember(Name="userId")>]
  userId : int
  [<field: DataMember(Name="accountId")>]
  accountId : int
}

[<DataContract>]
type ResetRequest = {
  [<field: DataMember(Name="token")>]
  token : string
  [<field: DataMember(Name="newpass")>]
  newpass : string
}

let fromJwtClaimToUserEntity (jwtClaimData: JwtClaim): UserEntity =
  {
    id = jwtClaimData.id;
    name = jwtClaimData.name;
    email = jwtClaimData.email;
    accountId = jwtClaimData.accountId;
    passhash = "";
  }


let isValidJwt (jwt: string): Result<UserEntity, string> =
  topLevelConfig
  |> Result.mapError (fun _ -> "")
  |> Result.bind (fun cfg ->
    try

      let jsonStr =
        JwtBuilder()
          .WithSecret(cfg.SecurityKey)
          .MustVerifySignature()
          .Decode(jwt)

      jsonStr
        |> System.Text.Encoding.UTF8.GetBytes
        |> fromJson
        |> fromJwtClaimToUserEntity
        |> Ok
    with Ex ->
      Error Ex.Message
  )
  |> Result.mapError (fun _ -> "Unauthenticated")

let private setJwtOnCtxWeb ctx jwt =
  match HttpContext.state ctx with
  | Some state ->
    state.set "authtoken" jwt
  | _ ->
    never

let setJwtCookie jwt =
  context (fun ctx ->
    stateful (Cookie.CookieLife.MaxAge <| TimeSpan.FromDays 7.0) true
    >=> setJwtOnCtxWeb ctx jwt
  )

let getTokenFromState (state: StateStore): string =
  defaultArg (state.get "authtoken") ""

let validateToken optStore =
  match optStore with
    | Some state ->
      state
      |> getTokenFromState
      |> isValidJwt
    | _ ->
      Error "Unauthenticated"

let checkLogin: WebPart =
  statefulForSession
  >=> context (HttpContext.state >> validateToken >> handleAuthResult)
  >=> setMimeType "application/json"

let withAuthenticatedUser f (req: HttpRequest) =
  statefulForSession
  >=> context (HttpContext.state >> validateToken >> (f req))


// Reset Code

type ResetEmailVerify = {
  email: string
  accountId: int
  userId: int
}

let fromJwtClaimToResetCreds (claim: ResetClaim): ResetEmailVerify =
  {
    email = claim.email
    accountId = claim.accountId
    userId = claim.userId
  }

let verifyReset (realResetCode, incomingResetCode) =
  if realResetCode = incomingResetCode then
    Ok ()
  else
    Error "Invalid Reset Code"

let handleJwtDecoding (jwt: string): Result<ResetEmailVerify, string> =
  topLevelConfig
  |> Result.mapError (fun _ -> "")
  |> Result.bind (fun cfg ->
    try

      let jsonStr =
        JwtBuilder()
          .WithSecret(cfg.SecurityKey)
          .MustVerifySignature()
          .Decode(jwt)

      jsonStr
        |> System.Text.Encoding.UTF8.GetBytes
        |> fromJson
        |> fromJwtClaimToResetCreds
        |> Ok
    with Ex ->
      Error Ex.Message
  )
  |> Result.mapError (fun _ -> "Invalid Token")
