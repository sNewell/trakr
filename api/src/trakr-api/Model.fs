module Model

open Suave.Json
open System.Runtime.Serialization


[<Literal>]
let LEFT = "left"
[<Literal>]
let RIGHT = "right"
[<Literal>]
let BOTTLE = "bottle"
[<Literal>]
let POO = "poo"
[<Literal>]
let PEE = "pee"
[<Literal>]
let BOTH = "both"
[<Literal>]
let FEED = "feed"
[<Literal>]
let BATHROOM = "potty"

type Side =
  | Left
  | Right
  | Bottle

type PottyType =
  | Poo
  | Pee
  | Both

type Activity =
  | Feeding of Side * int64 * int
  | Bathroom of PottyType * int64 * int

let withId id act =
  match act with
  | Feeding (side, time, _) ->
    Feeding (side, time, id)
  | Bathroom (pt, time, _) ->
    Bathroom (pt, time, id)


[<DataContract>]
type ActivityJson = {
  [<field: DataMember(Name="activity")>]
    activityType : string
  [<field: DataMember(Name="qualifier")>]
    subType : string
  [<field: DataMember(Name="time")>]
    time : int64
  [<field: DataMember(Name="id")>]
    id : int
}

[<DataContract>]
type StatsJson = {
  [<field: DataMember(Name="average")>]
    average : int
  [<field: DataMember(Name="high")>]
    high : int
  [<field: DataMember(Name="low")>]
    low : int
}

/// Register a new account with a new login
[<DataContract>]
type RegisterJson = {
  [<field: DataMember(Name="accessCode")>]
    accessCode : string
  [<field: DataMember(Name="email")>]
    email : string
  [<field: DataMember(Name="password")>]
    password : string
  [<field: DataMember(Name="familyName")>]
    familyName : string
  [<field: DataMember(Name="name")>]
    name : string
}

/// Adds a user to an existing account, requires re-auth
/// from an existing login on the account
[<DataContract>]
type AddLoginJson = {
  [<field: DataMember(Name="accountId")>]
    accountId : int
  [<field: DataMember(Name="authorizingUserId")>]
    authorizingUserId : int
  [<field: DataMember(Name="authorizingUserPass")>]
    authorizingUserPass : int
  [<field: DataMember(Name="email")>]
    email : string
  [<field: DataMember(Name="password")>]
    password : string
}

[<DataContract>]
type LoginJson = {
  [<field: DataMember(Name="email")>]
    email : string
  [<field: DataMember(Name="password")>]
    password : string
}

[<DataContract>]
type LoginTokenResponseJson = {
  [<field: DataMember(Name="id")>]
    id : int
  [<field: DataMember(Name="succeeded")>]
    succeeded : bool
}

[<DataContract>]
type ResetRequestJson = {
  [<field: DataMember(Name="token")>]
    token : string
  [<field: DataMember(Name="newPass")>]
    newPass : string
  [<field: DataMember(Name="resetCode")>]
    resetCode : string
}

type ActivityEntity = {
  id : int
  name : string
  qualifier : string
  time : int64
  accountId : int
}

let mkActEntity n q t i a =
  { name = n; qualifier = q; time = t; id = i; accountId = a }


type StatsEntity = {
  average : decimal
  max : int64
  min : int64
}

type UserEntity = {
  id : int
  accountId : int
  email : string
  passhash : string
  name : string
}

[<DataContract>]
type UserEntityJson = {
  [<field: DataMember(Name="id")>]
  id : int
  [<field: DataMember(Name="accountId")>]
  accountId : int
  [<field: DataMember(Name="email")>]
  email : string
  [<field: DataMember(Name="name")>]
  name : string
}

let ueToJson (ue: UserEntity) =
  {
    id = ue.id
    accountId = ue.accountId
    email = ue.email
    name = ue.name
  }

type AccountEntity = {
  id : int
  accountname : string
}

type LoginEntity = {
  accountId : int
  userId : int
}


let prepActivityForJson act =
  match act with
  | Feeding(side, time, id) ->
    match side with
    | Left ->
      { activityType = FEED
        subType = LEFT
        time = time
        id = id }
    | Right ->
      { activityType = FEED
        subType = RIGHT
        time = time
        id = id }
    | Bottle ->
      { activityType = FEED
        subType = BOTTLE
        time = time
        id = id }
  | Bathroom(subtype, time, id) ->
    match subtype with
    | Poo ->
      { activityType = BATHROOM
        subType = POO
        time = time
        id = id }
    | Pee ->
      { activityType = BATHROOM
        subType = PEE
        time = time
        id = id }
    | Both ->
      { activityType = BATHROOM
        subType = BOTH
        time = time
        id = id }

let convertFromJson (act: ActivityJson): Result<Activity, string> =
  match act.activityType with
  | FEED ->
    match act.subType with
    | LEFT ->
      Ok <| Feeding(Left, act.time, act.id)
    | RIGHT ->
      Ok <| Feeding(Right, act.time, act.id)
    | BOTTLE ->
      Ok <| Feeding(Bottle, act.time, act.id)
    | _ ->
      Error "Invalid feed qualifier"
  | BATHROOM ->
    match act.subType with
    | POO ->
      Ok <| Bathroom(Poo, act.time, act.id)
    | PEE ->
      Ok <| Bathroom(Pee, act.time, act.id)
    | BOTH ->
      Ok <| Bathroom(Both, act.time, act.id)
    | _ ->
      Error "Invalid potty qualifier"
  | _ ->
    Error "Invald activity"


let actFromEntity (ae: ActivityEntity): Result<Activity, string> =
  match ae.name with
  | FEED ->
    match ae.qualifier with
    | LEFT ->
      Ok <| Feeding(Left, ae.time, ae.id)
    | RIGHT ->
      Ok <| Feeding(Right, ae.time, ae.id)
    | BOTTLE ->
      Ok <| Feeding(Bottle, ae.time, ae.id)
    | _ ->
      Error "Invalid feed qualifier"
  | BATHROOM ->
    match ae.qualifier with
    | POO ->
      Ok <| Bathroom(Poo, ae.time, ae.id)
    | PEE ->
      Ok <| Bathroom(Pee, ae.time, ae.id)
    | BOTH ->
      Ok <| Bathroom(Both, ae.time, ae.id)
    | _ ->
      Error "Invalid potty qualifier"
  | _ ->
    Error "Invald activity"

let actToEntity accId (a: Activity): ActivityEntity =
  match a with
  | Feeding(Left, time, id) ->
    mkActEntity FEED LEFT time id accId
  | Feeding(Right, time, id) ->
      mkActEntity FEED RIGHT time id accId
  | Feeding(Bottle, time, id) ->
      mkActEntity FEED BOTTLE time id accId
  | Bathroom(Poo, time, id) ->
      mkActEntity BATHROOM POO time id accId
  | Bathroom(Pee, time, id) ->
      mkActEntity BATHROOM PEE time id accId
  | Bathroom(Both, time, id) ->
      mkActEntity BATHROOM BOTH time id accId

let actToUtf8Json =
  prepActivityForJson
  >> toJson
  >> System.Text.Encoding.UTF8.GetString

let userEntityToUtf8Json =
  ueToJson
  >> toJson
  >> System.Text.Encoding.UTF8.GetString