/// All the SQL you could ask for
module SqlStrs

[<Literal>]
let LastFiftyActivitiesSql = @"
SELECT id, name, qualifier, time, accountId
FROM trakr.activities
WHERE accountId = @accountId
ORDER BY time
DESC LIMIT 50;
"

[<Literal>]
let ActivityByIdSql = @"SELECT * FROM trakr.activities WHERE id = @id;"

[<Literal>]
let InsertActivitySql = @"
INSERT INTO trakr.activities(name, qualifier, time, accountId)
VALUES (@name, @qualifier, @time, @accountId)
RETURNING id;
"

[<Literal>]
let UpdateActivitySql = @"
UPDATE trakr.activities
SET
  name = @name,
  qualifier = @qualifier,
  time = @time
WHERE id = @id;
"

[<Literal>]
let DeleteActivitySql = @"
DELETE FROM trakr.activities
WHERE id = @id;
"

[<Literal>]
let StatsSql = @"
SELECT
  AVG(numPerDay) as average,
  MAX(numPerDay) as max,
  MIN(numPerDay) as min
FROM
  (
    SELECT COUNT(true) as numPerDay
    FROM
    (
      SELECT EXTRACT(DAY FROM to_timestamp(time)) as day
      FROM trakr.activities
      WHERE
        accountId = @accountId
        AND time BETWEEN @fromutc AND @toutc
        AND name='potty'
        AND (qualifier='pee' OR qualifier='both')
    ) as days
    GROUP BY day
  ) as counts;
"

[<Literal>]
let FetchPassHashByEmailSql = @"
SELECT u.id, l.accountId, u.email, u.passhash, u.name
FROM trakr.users u
JOIN trakr.logins l ON u.id = l.userId
WHERE u.email = @email;
"

[<Literal>]
let FetchUserEntityByUserId = @"
SELECT u.id, l.accountId, u.email, u.passhash, u.name
FROM trakr.users u
JOIN trakr.logins l ON u.id = l.userId
WHERE u.id = @id;
"

[<Literal>]
let UpdateUserPassHash = @"
UPDATE trakr.users
SET passhash = @newPassHash
WHERE id = @id;
"
