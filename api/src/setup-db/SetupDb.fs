module SetupDb

open System
open System.Data
open Npgsql
open Dapper
open SetupConfig
open AccountSetup


let (>>=) res f =
  res |> Result.bind f

let (||>) res f =
  res |> Result.map f

let (|?>) res errF =
  res |> Result.mapError errF



type DbMetaData = {
  name : string
}

let mkSuperConnStr cfg =
  sprintf
    "Host=%s;Port=%d;Database=%s;Username=%s;Password=%s"
    cfg.Host
    cfg.Port
    cfg.SuperDb
    cfg.SuperUser
    cfg.SuperUserPass

let mkConnStr cfg =
  sprintf
    "Host=%s;Port=%d;Database=%s;Username=%s;Password=%s"
    cfg.Host
    cfg.Port
    cfg.SuperDb
    cfg.User
    cfg.UserPass

let withConn f connStr =
  let conn = new NpgsqlConnection(connStr)
  conn.Open()
  let toRet = f conn
  conn.Dispose()
  toRet

let dbExecute (sql: string) (data: _) (db: IDbConnection) = 
  db.Execute(sql, data)


let dbUsersSql = @"
SELECT usename as name FROM pg_user;
"

let createUserSql = @"
CREATE USER %%USER%% WITH PASSWORD %%PASS%%;
"

let createSchemaSql = @"
CREATE SCHEMA IF NOT EXISTS %%SCHEMA%% AUTHORIZATION %%USER%%;
"

type CountColsParams = {
  tableName : string
}

type NumOfCols = {
  numOfCols : int64
}

let countColsOnTableSql = @"
SELECT COUNT(true) AS numOfCols
FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = @tableName;
"

let countColsParamActivity = {
  tableName = "activities"
}

let countItemsFromTableSql = @"
SELECT COUNT(true) AS num
FROM trakr.%%TABLE%%
WHERE %%PRED%%;
"

type Count = {
  num : int64
}

let countAccountsSql =
  countItemsFromTableSql
    .Replace("%%TABLE%%", "accounts")
    .Replace("%%PRED%%", "id = 1");

let activityColumns = @"
  name varchar(255),
  qualifier varchar(255),
  time bigint
"

let accountColumns = @"
  accountName text
"

let userColumns = @"
  email text UNIQUE,
  passhash text,
  name text
"

let loginsColumns = @"
  accountId integer,
  userId integer,
  FOREIGN KEY (accountId) REFERENCES trakr.accounts,
  FOREIGN KEY (userId) REFERENCES trakr.users
"

let createTableSql = @"
CREATE TABLE IF NOT EXISTS %%SCHEMA%%.%%TABLE%%(
  id SERIAL PRIMARY KEY,
  %%COLS%%
);
"

let alterTableSql = @"
ALTER TABLE %%SCHEMA%%.%%TABLE%%
%%ALTERS%%
"

let createJoinTableSql = @"
CREATE TABLE IF NOT EXISTS %%SCHEMA%%.%%TABLE%%(
  %%COLS%%
);
"

type InterrColParam = {
  tableName : string
  columnName : string
}

let interrogateColumnData = @"
SELECT is_nullable, data_type, column_default
FROM information_schema.columns
WHERE table_name = @tableName
  AND column_name = @columnName;
"

type ConstraintParam = {
  con : string
}

let constraintExistsSql = @"
SELECT COUNT(true)
FROM information_schema.referential_constraints
WHERE constraint_name = @con;
"

let activityAddAccountIdSafeAlters = @"
ADD COLUMN IF NOT EXISTS accountId INTEGER;
"

let newActivityConstraintAlters = @"
ALTER COLUMN accountId SET DEFAULT 0,
ALTER COLUMN accountId SET NOT NULL,
DROP CONSTRAINT IF EXISTS account_fk,
ADD CONSTRAINT account_fk FOREIGN KEY (accountId) REFERENCES trakr.accounts;
"

let hasUser (db: IDbConnection) () =
  db.Query<DbMetaData>(dbUsersSql, ())

let exCreateUser (db: IDbConnection) user pass () =
  db.Execute(
    createUserSql
      .Replace("%%USER%%", user)
      .Replace("%%PASS%%", sprintf "'%s'" pass),
    ())

// inject the db parts (makes this function testable)
let createUserIfNeeded queryForUser createUser cfg =
  let hasUser =
    queryForUser ()
    |> Seq.exists (fun d -> d.name = cfg.User)

  if hasUser then
    printfn "Postgres app db User already created."
    Ok true
  else
    try
      let numOfRows = createUser ()
      if numOfRows = -1 then
        Ok true
      else
        Error "Failed to create Postgres app db User"
    with Ex ->
      Error <| sprintf "Failed to create Postgres app db User: %s" Ex.Message


let exCreateSchema (db: IDbConnection) dbname user () =

  let schemaSql =
    createSchemaSql
      .Replace("%%SCHEMA%%", dbname)
      .Replace("%%USER%%", user)

  db.Execute(schemaSql, ())

let exCreateTable (db: IDbConnection) dbname cols table =

  let createTableSql =
    createTableSql
      .Replace("%%SCHEMA%%", dbname)
      .Replace("%%TABLE%%", table)
      .Replace("%%COLS%%", cols)

  db.Execute(createTableSql, ())

let exAlterTable (db: IDbConnection) dbname alters table =
  let alterSql =
    alterTableSql
      .Replace("%%SCHEMA%%", dbname)
      .Replace("%%TABLE%%", table)
      .Replace(@"%%ALTERS%%", alters)
  db.Execute(alterSql, ())

let exCreateJoinTable (db: IDbConnection) dbname cols table =
  db.Execute(
    createJoinTableSql
      .Replace("%%SCHEMA%%", dbname)
      .Replace("%%TABLE%%", table)
      .Replace("%%COLS%%", cols),
    ()
  )

let createSchemaIfNeeded createSchema shouldAttempt =
  if shouldAttempt then
    try
      let numOfRows = createSchema ()
      if numOfRows = -1 then
        Ok true
      else
        Error "Failed to create db"
    with Ex ->
      Error <| sprintf "Failed to create db with err: %s" Ex.Message 
  else
    Ok false

let createTableIfNeeded mkTable tablename shouldAttempt =
  if shouldAttempt then
    try 
      let numOfRows = mkTable tablename
      if numOfRows = -1 then
        Ok true
      else
        Error <| sprintf "Failed to create table %s" tablename
    with Ex ->
      Error <| sprintf "Failed to create table %s with err: %s" tablename Ex.Message
  else
    Ok false

let createTable mkTableFunc tablename _ =
  createTableIfNeeded mkTableFunc tablename true

let alterTable alterFunc table _ =
  try
    let alterResponse = alterFunc table
    if alterResponse = -1 then
      Ok true
    else
      Error <| sprintf "Got back %d from ALTER command, aborting" alterResponse
  with Ex ->
    Error <| sprintf "Failed to alter table with err: %s" Ex.Message


let handleResult (res: Result<'t, string>) =
  match res with
  | Ok _ ->
    printfn "Database setup all proper. 😺"
    0
  | Error msg ->
    printfn "Setting up the db failed:\n\t%s" msg
    1

let exQuery (db: IDbConnection) (sql: string) (sqlParams: obj) () =
  db.Query<_>(sql, sqlParams)


let demoAccountSql = @"
INSERT INTO trakr.accounts(accountname)
VALUES ('demo account');
"

// hash of password 'pass' given 'default' user in trakr
// AQAAAAEAACcQAAAAEBBUEKsW9d27cymaegl95yjpVsdmvIiqFCpsq4xo2wRDVHUTLAI8clo7O0DWegUKCw==

// The test@test.com user's password is 'pass'
let demoUserSql = @"
INSERT INTO trakr.users(email, passhash, name)
VALUES (
  'test@test.com',
  'AQAAAAEAACcQAAAAEBBUEKsW9d27cymaegl95yjpVsdmvIiqFCpsq4xo2wRDVHUTLAI8clo7O0DWegUKCw==',
  'Test'
);
"

let demoLoginSql = @"
INSERT INTO trakr.logins(accountId, userId)
VALUES (1,1);
"

let createDemoData (db: IDbConnection) _ =
  let accountCount: Count = exQuery db countAccountsSql () () |> Seq.head
  if accountCount.num = (int64)1 then
    printfn "Demo account already created."
    Ok ()
  else
    let accRow = db.Execute(demoAccountSql, ()) 
    if accRow <> 1 then
      Error "Failed to insert demo account."
    else 
      let userRow = db.Execute(demoUserSql, ())
      if userRow <> 1 then
        Error "Failed to insert demo user"
      else
        let loginRow = db.Execute(demoLoginSql, ())
        if loginRow <> 1 then
          Error "Failed to insert demo login"
        else
          Ok ()

type UpdateAccId = {
  accountId : int
}

let updateAccSql = @"
UPDATE trakr.activities
SET accountId = @accountId; 
"

let insertNewells (db: IDbConnection) =
  let newellUserId = db.ExecuteScalar<int>("INSERT INTO trakr.users(email, passhash, name) VALUES ('sean@thenewells.us', '', 'Newells') RETURNING id;")
  let newellAccId = db.ExecuteScalar<int>("INSERT INTO trakr.accounts(accountname) VALUES('The Newells') RETURNING id;")

  if newellUserId > 0 || newellAccId > 0 then
    db.Execute(sprintf "INSERT INTO trakr.logins(accountId, userId) VALUES (%d, %d)" newellAccId newellUserId ) |> ignore
    Ok (db, newellAccId, newellUserId)
  else
    Error "Failed to insert newell account"

let updateActsForNewells (db: IDbConnection, accId: int, userId: int) =
  let numORows = db.Execute(updateAccSql, { accountId = accId })
  if numORows >= 0 then
    Ok (db, accId, userId)
  else
    Error "Error updating existing activites for the newells"

let emailNewells (cfg: Config) (db: IDbConnection, accId: int, userId: int) =
  let jwtPayload: ResetEmailVerify = {
    email = "sean@thenewells.us";
    accountId = accId;
    userId = userId
  }

  let resetLink = genJwt ( cfg.SecurityKey, jwtPayload ) |> mkLink

  sendPasswordResetEmail cfg "Newells" resetLink jwtPayload.email "Set your Trakr password 🔐"

  Ok db

let insertKenyons (db: IDbConnection) =
  let kenyonUserId = db.ExecuteScalar<int>("INSERT INTO trakr.users(email, passhash, name) VALUES ('morganwkenyon@gmail.com', '', 'Kenyons') RETURNING id;")
  let kenyonAccId = db.ExecuteScalar<int>("INSERT INTO trakr.accounts(accountname) VALUES('The Kenyons') RETURNING id;")

  if kenyonUserId > 0 && kenyonAccId > 0 then
    db.Execute(sprintf "INSERT INTO trakr.logins(accountId, userId) VALUES (%d, %d)" kenyonAccId kenyonUserId ) |> ignore
    Ok (db, kenyonAccId, kenyonUserId)
  else
    Error "Failed to insert kenyon account"

let emailKenyons cfg (db: IDbConnection, accId: int, userId: int) =
  let jwtPayload: ResetEmailVerify = {
    email = "morganwkenyon@gmail.com";
    accountId = accId;
    userId = userId
  }

  let resetLink = genJwt ( cfg.SecurityKey, jwtPayload ) |> mkLink

  sendPasswordResetEmail cfg "Kenyons" resetLink jwtPayload.email  "Set your Trakr password 🔐"

  Ok db

let createInitialAccountsAndData (cfg: Config) (db: IDbConnection) _ =
  let hasAccFk = db.ExecuteScalar<int>(constraintExistsSql, { con = "account_fk" }) = 1
  if hasAccFk then
    printfn "Newells and Kenyons already created"
    Ok ()
  else
    printfn "Setting up Newells and Kenyons"
    db
    |> insertNewells
    >>= updateActsForNewells
    >>= emailNewells cfg
    >>= insertKenyons
    >>= emailKenyons cfg
    ||> ignore

let setupDb cfg =
  cfg
  |> mkSuperConnStr
  |> withConn (fun db ->
    createUserIfNeeded (hasUser db) (exCreateUser db cfg.User cfg.UserPass) cfg
    >>= createSchemaIfNeeded (exCreateSchema db cfg.Schema cfg.User)
  )
  ||> (fun _ -> mkConnStr cfg)
  >>= withConn (fun db ->
    createTable (exCreateTable db cfg.Schema activityColumns) "activities" ()
    >>= createTable (exCreateTable db cfg.Schema accountColumns) "accounts"
    >>= createTable (exCreateTable db cfg.Schema userColumns) "users"
    >>= createTable (exCreateJoinTable db cfg.Schema loginsColumns) "logins"
    >>= createDemoData db
    >>= alterTable (exAlterTable db cfg.Schema activityAddAccountIdSafeAlters) "activities"
    >>= createInitialAccountsAndData cfg db
    >>= alterTable (exAlterTable db cfg.Schema newActivityConstraintAlters) "activities"
  )
  |> handleResult


let loadEnvIfNeeded (args: string array) =
  if args.Length > 0 && IO.File.Exists args.[0] && System.IO.Path.GetExtension(args.[0]) = ".env" then
    IO.File.ReadAllLines args.[0]
    |> Array.map (fun l ->
      let parts = l.Split('=')
      if parts.Length <> 2 then
        printfn "Rejected line '%s'" l
        ()
      else
        Environment.SetEnvironmentVariable(parts.[0], parts.[1])
        ()
    )
    |> ignore
  else
    ()

[<EntryPoint>]
let main args =

  loadEnvIfNeeded args

  withConfig
    setupDb
    (fun errMsg ->
      printfn "*** Config Error!"
      printfn "***  %s" errMsg
      1
    )
