module SetupConfig

open FsConfig

[<Convention("PG")>]
type Config = {
  [<DefaultValue("localhost")>]
  Host : string
  [<DefaultValue("54320")>]
  Port : int
  [<DefaultValue("postgres")>]
  SuperUser : string
  [<DefaultValue("pass")>]
  SuperUserPass : string
  [<DefaultValue("postgres")>]
  SuperDb : string
  [<DefaultValue("trakr")>]
  Schema : string
  [<DefaultValue("trakrapp")>]
  User : string
  [<DefaultValue("pass")>]
  UserPass : string
  [<DefaultValue("notaverysecurekey")>]
  SecurityKey : string
  [<DefaultValue("sandbox418d640d27b7438cbd57f69b56f1770f.mailgun.org")>]
  MailgunDomain : string
  [<DefaultValue("app")>]
  MailgunFromUser : string
  MailgunApiKey : string
}

let withConfig f g =
  match EnvConfig.Get<Config>() with
  | Ok cfg ->
    f cfg
  | Error err ->
    let errMsg =
      match err with
      | NotFound name ->
        sprintf "\tConfig not found '%s'" name
      | BadValue (name, envValue) ->
        sprintf "\tBad value for '%s' - %s" name envValue
      | NotSupported msg ->
        sprintf "\tUnsupported: %s" msg
    g errMsg
