#!/bin/sh

# Helpful script to exec some SQL into postgres running in the container

# Docker Exec Admin (run as the postgres super user)
dexAdmin() {
  docker exec trakr-postgres-db psql -U postgres -c "$1"
}

# Docker Exec sql (as the normal application user)
dexTrakr() {
  docker exec trakr-postgres-db psql -U trakrapp -d postgres -c "$1"
}

#dexTrakr "SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'trakr';"
dexTrakr "SELECT column_name, data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'logins';"
