module AccountSetup

open Microsoft.Extensions.Options
open Microsoft.AspNetCore.Identity
open JWT.Builder
open JWT.Algorithms
open System
open FSharp.Data
open SetupEmailTemplates
open SetupConfig

let wrapOpts a =
  Options.Create a

type ResetEmailVerify = {
  email: string
  accountId: int
  userId: int
}

let genJwt (key: string, creds: ResetEmailVerify): string =
  JwtBuilder()
    .WithAlgorithm(HMACSHA256Algorithm())
    .WithSecret([| key |])
    .AddClaim("exp", DateTimeOffset.UtcNow.AddDays(2|>float).ToUnixTimeSeconds())
    .AddClaim("accountId", creds.accountId)
    .AddClaim("userId", creds.userId)
    .AddClaim("email", creds.email)
    .Build()

let mkLink (jwt: string) =
  sprintf "https://trakr.thenewells.us/reset#%s" jwt // TODO do we want this configurable?

// Email stuff

let private mailGunRoot = "https://api.mailgun.net/v3/"

let private mkEmailAddress fromUser domain =
  sprintf "%s <%s@%s>" fromUser fromUser domain

let private mkMessageUrl baseUrl domain =
  sprintf "%s%s/messages" baseUrl domain

let private mkMailgunAuth key = sprintf "%s:%s" "api" key
let private mkEncodedMailgunAuth key = System.Text.Encoding.UTF8.GetBytes(mkMailgunAuth key) |> System.Convert.ToBase64String
let private mkMailgunAuthHeaders key = [ ( "Authorization", "Basic " + (mkEncodedMailgunAuth key) ) ]

let private mkMailgunMessageUrl domain = mkMessageUrl mailGunRoot domain

/// Sends an application email from trakr with the subject and body as a plain text email
let sendMailPlain (cfg: Config) recip subject body =
  let url = mkMailgunMessageUrl cfg.MailgunDomain
  let fromAddress = mkEmailAddress cfg.MailgunFromUser cfg.MailgunDomain
  let authHeaders = mkMailgunAuthHeaders cfg.MailgunApiKey

  Http.RequestString
   ( url,
    body =
      FormValues [
        ("from", fromAddress)
        ("to", recip)
        ("subject", subject)
        ("text", body)
      ],
    headers = authHeaders
  )

let sendMailHtml (cfg: Config) recip subject htmlBod =
  let url = mkMailgunMessageUrl cfg.MailgunDomain
  let fromAddress = mkEmailAddress cfg.MailgunFromUser cfg.MailgunDomain
  let authHeaders = mkMailgunAuthHeaders cfg.MailgunApiKey

  Http.RequestString
   ( url,
    body =
      FormValues [
        ("from", fromAddress)
        ("to", recip)
        ("subject", subject)
        ("html", htmlBod)
      ],
    headers = authHeaders
  )

let sendPasswordResetEmail cfg accName link recip subject =
  mkPasswordResetEmail accName link
  |> sendMailHtml cfg recip subject
  |> ignore // Test for 200? Will Http.RequestString fail? 

