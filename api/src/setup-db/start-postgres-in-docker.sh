#!/bin/sh

# Spawn a postgres docker container locally
# names trakr-postgres-db,
# with default password 'pass',
# in the background,
# exposing port 54320 on localhost,
# with the data in the db coming from a volume (trakr-db),
# using v12 of postgres on an alpine linux base

docker run --name trakr-postgres-db \
  -e POSTGRES_PASSWORD=pass \
  -d \
  -p 54320:5432 \
  -v trakr-db:/var/lib/postgresql/data \
  postgres:12-alpine