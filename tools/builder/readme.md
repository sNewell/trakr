# Builder Tool

This docker file builds an image that is used in the
CI/CD pipeline. The image is a slimmed down image that
is able to build the elm and f# side of the project.
