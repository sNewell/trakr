# Trakr

The baby stat tracker, for tracking:
- Feedings
- Diapers
- Sleeps (TODO)

## Structure

The `api` folder holds the backend that the front end talks to.
The `app` folder holds the front end that users interact with.

